package com.fraczekkrzysztof.resourceretriever.resource.fileusecase;

import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.fileusecase.model.FileResource;
import com.fraczekkrzysztof.resourceretriever.resource.fileusecase.repository.FileResourceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FileResourceRetrieverTest {

    @Mock
    ResourceQueue<String> fileResourceQueue;
    @Mock
    FileResourceRepository fileResourceRepository;
    @Mock
    RestTemplate restTemplateForFileResource;

    @InjectMocks
    FileResourceRetriever fileResourceRetriever;

    @Test
    void shouldRetrieveFileResourceAndStoreItInDB() {
        //given
        String httpPath = "https://www.google.com/file.pdf";
        given(fileResourceQueue.getElement()).willReturn(Optional.of(httpPath));
        given(restTemplateForFileResource.exchange(eq(httpPath), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class))).willReturn(new ResponseEntity<>(new byte[]{}, HttpStatus.OK));
        FileResource expectedFileResourceToStore = new FileResource();
        expectedFileResourceToStore.setHttpPath(httpPath);
        expectedFileResourceToStore.setFileResource(new byte[]{});

        //when
        fileResourceRetriever.retrieveResource();

        //then
        verify(fileResourceRepository, times(1)).save(expectedFileResourceToStore);
    }

    @Test
    void shouldDoNothingWhenThereIsNoElementsOnQueue() {
        //given
        given(fileResourceQueue.getElement()).willReturn(Optional.empty());

        //when
        fileResourceRetriever.retrieveResource();

        //then
        verifyNoInteractions(fileResourceRepository);
        verifyNoInteractions(restTemplateForFileResource);
    }

}