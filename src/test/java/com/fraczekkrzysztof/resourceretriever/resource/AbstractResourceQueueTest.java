package com.fraczekkrzysztof.resourceretriever.resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AbstractResourceQueueTest {

    @Spy
    ConcurrentLinkedQueue<String> queue;

    private ResourceQueue<String> resourceQueue;

    @BeforeEach
    void setUp() {
        resourceQueue = new AbstractResourceQueue<>(queue) {
        };
    }

    @Test
    void shouldAddElementToQueue() {
        //when
        resourceQueue.addElement("SomeString");

        //then
        verify(queue).add("SomeString");
        assertThat(queue.size()).isEqualTo(1);
    }

    @Test
    void
    shouldGetElementFromQueue() {
        //given
        queue.add("SomeString");

        //when
        Optional<String> result = resourceQueue.getElement();

        //then
        verify(queue).poll();
        assertThat(result).hasValue("SomeString");
        assertThat(queue.size()).isZero();
    }

    @Test
    void
    shouldReturnEmptyOptionalIfQueueIsEmpty() {
        //when
        Optional<String> result = resourceQueue.getElement();

        //then
        verify(queue).poll();
        assertThat(result).isEmpty();
        assertThat(queue.size()).isZero();
    }

}