package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase;

import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.model.HtmlResource;
import com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.repository.HtmlResourceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class HtmlResourceRetrieverTest {

    @Mock
    ResourceQueue<String> htmlResourceQueue;
    @Mock
    HtmlResourceRepository htmlResourceRepository;
    @Mock
    RestTemplate restTemplateForHtmlResource;

    @InjectMocks
    HtmlResourceRetriever htmlResourceRetriever;

    @Test
    void shouldRetrieveHtmlResourceAndStoreItInDB() {
        //given
        String httpPath = "https://www.google.com";
        given(htmlResourceQueue.getElement()).willReturn(Optional.of(httpPath));
        given(restTemplateForHtmlResource.getForEntity(httpPath, String.class)).willReturn(new ResponseEntity<>("Some body value", HttpStatus.OK));
        HtmlResource expectedHtmlResourceToStore = new HtmlResource();
        expectedHtmlResourceToStore.setHttpPath(httpPath);
        expectedHtmlResourceToStore.setHtmlResource("Some body value");

        //when
        htmlResourceRetriever.retrieveResource();

        //then
        verify(htmlResourceRepository, times(1)).save(expectedHtmlResourceToStore);
    }

    @Test
    void shouldDoNothingWhenThereIsNoElementsOnQueue() {
        //given
        given(htmlResourceQueue.getElement()).willReturn(Optional.empty());

        //when
        htmlResourceRetriever.retrieveResource();

        //then
        verifyNoInteractions(htmlResourceRepository);
        verifyNoInteractions(restTemplateForHtmlResource);
    }


}