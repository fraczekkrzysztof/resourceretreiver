package com.fraczekkrzysztof.resourceretriever.validator;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


class DefaultUrlValidatorTest {

    private UrlValidator urlValidator = new DefaultUrlValidator();

    @Test
    void shouldNotThrowAnyExceptionWhenValidUrlIsPassedAsArgument() {
        //when
        ThrowableAssert.ThrowingCallable call = () -> urlValidator.validateUrl("https://www.google.pl");

        //then
        assertThatCode(call).doesNotThrowAnyException();
    }

    @Test
    void shouldThrowUrlValidationExceptionWhenInvalidUrlIsPassed() {
        //when
        ThrowableAssert.ThrowingCallable call = () -> urlValidator.validateUrl("I'm invalid url");

        //then
        assertThatExceptionOfType(UrlValidationException.class)
                .isThrownBy(call)
                .withMessage("Provided url \"I'm invalid url\" is nor valid")
                .withCauseInstanceOf(MalformedURLException.class);
    }

}