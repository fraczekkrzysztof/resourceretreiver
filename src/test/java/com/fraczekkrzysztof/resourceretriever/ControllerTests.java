package com.fraczekkrzysztof.resourceretriever;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fraczekkrzysztof.resourceretriever.dto.ResourceRequestDto;
import com.fraczekkrzysztof.resourceretriever.exceptionhandler.ErrorDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    void shouldPerformCallToHtmlResource() {
        //given
        objectMapper.registerModule(new JavaTimeModule());
        String url = "http://localhost:" + port + "/resource/html";
        ResourceRequestDto requestDto = new ResourceRequestDto();
        requestDto.setUrl("https://wwww.google.com");

        //when
        ResponseEntity<String> response = restTemplate.postForEntity(url, requestDto, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void shouldPerformCallToFileResource() {
        //given
        String url = "http://localhost:" + port + "/resource/file";
        ResourceRequestDto requestDto = new ResourceRequestDto();
        requestDto.setUrl("https://prawie.pro/wp-content/uploads/2021/03/Formularz-zwrotu-1.pdf");

        //when
        ResponseEntity<String> response = restTemplate.postForEntity(url, requestDto, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void shouldReturnHttp400WithInformationThatUrlInBodyIsRequired() throws JsonProcessingException {
        //given
        String url = "http://localhost:" + port + "/resource/html";
        ResourceRequestDto requestDto = new ResourceRequestDto();
        ErrorDto expectedError = new ErrorDto(null, 400, "Please provide a valid URL", "uri=/resource/html");

        //when
        ResponseEntity<String> response = restTemplate.postForEntity(url, requestDto, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        ErrorDto errorDto = objectMapper.readValue(response.getBody(), ErrorDto.class);
        assertThat(errorDto.getTimestamp()).isAfterOrEqualTo(LocalDateTime.now().minus(1, ChronoUnit.MINUTES)).isBefore(LocalDateTime.now());
        assertThat(errorDto).usingRecursiveComparison().ignoringFields("timestamp").isEqualTo(expectedError);
    }

    @Test
    void shouldReturnHttp400WithInformationThatUrlInBodyIsInvalid() throws JsonProcessingException {
        //given
        String url = "http://localhost:" + port + "/resource/file";
        ResourceRequestDto requestDto = new ResourceRequestDto();
        requestDto.setUrl("invalid");
        ErrorDto expectedError = new ErrorDto(null, 400, "Provided url \"invalid\" is nor valid", "uri=/resource/file");

        //when
        ResponseEntity<String> response = restTemplate.postForEntity(url, requestDto, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        ErrorDto errorDto = objectMapper.readValue(response.getBody(), ErrorDto.class);
        assertThat(errorDto.getTimestamp()).isAfterOrEqualTo(LocalDateTime.now().minus(1, ChronoUnit.MINUTES)).isBefore(LocalDateTime.now());
        assertThat(errorDto).usingRecursiveComparison().ignoringFields("timestamp").isEqualTo(expectedError);
    }
}
