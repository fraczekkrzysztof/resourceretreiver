package com.fraczekkrzysztof.resourceretriever.exceptionhandler;

import com.fraczekkrzysztof.resourceretriever.validator.UrlValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = UrlValidationException.class)
    public ResponseEntity<ErrorDto> handleUrlValidationException(UrlValidationException exception, WebRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorDto error = new ErrorDto(LocalDateTime.now(), status.value(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        String message = exception.getAllErrors().get(0).getDefaultMessage();
        ErrorDto error = new ErrorDto(LocalDateTime.now(), status.value(), message, request.getDescription(false));
        return new ResponseEntity(error, status);
    }
}
