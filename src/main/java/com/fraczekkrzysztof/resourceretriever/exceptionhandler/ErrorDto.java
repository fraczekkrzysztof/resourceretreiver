package com.fraczekkrzysztof.resourceretriever.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ErrorDto {

    LocalDateTime timestamp;
    int status;
    String error;
    String path;
}
