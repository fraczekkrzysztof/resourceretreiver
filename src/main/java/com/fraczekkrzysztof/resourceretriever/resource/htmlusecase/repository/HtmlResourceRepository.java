package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.repository;

import com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.model.HtmlResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HtmlResourceRepository extends JpaRepository<HtmlResource, Integer> {
}
