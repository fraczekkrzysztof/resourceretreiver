package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class HtmlResource {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "httpPath", updatable = false)
    private String httpPath;

    @Lob()
    @Column(name = "htmlResource", updatable = false)
    private String htmlResource;

    @CreationTimestamp
    @Column(name = "createdAt", updatable = false)
    private LocalDateTime createdAt;
}
