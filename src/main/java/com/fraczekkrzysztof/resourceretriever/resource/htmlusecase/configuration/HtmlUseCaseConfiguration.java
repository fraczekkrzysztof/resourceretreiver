package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.configuration;

import com.fraczekkrzysztof.resourceretriever.resource.AbstractResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ConcurrentLinkedQueue;

@Configuration
public class HtmlUseCaseConfiguration {

    @Bean
    public RestTemplate restTemplateForHtmlResource() {
        return new RestTemplate();
    }

    @Bean
    public ResourceQueue<String> htmlResourceQueue() {
        return new AbstractResourceQueue<>(new ConcurrentLinkedQueue<>()) {
        };
    }
}
