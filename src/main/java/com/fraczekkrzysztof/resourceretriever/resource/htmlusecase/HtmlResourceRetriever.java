package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase;

import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceRetriever;
import com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.model.HtmlResource;
import com.fraczekkrzysztof.resourceretriever.resource.htmlusecase.repository.HtmlResourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HtmlResourceRetriever implements ResourceRetriever {

    private final ResourceQueue<String> htmlResourceQueue;
    private final HtmlResourceRepository htmlResourceRepository;
    private final RestTemplate restTemplateForHtmlResource;

    @Override
    @Scheduled(fixedRate = 1000)
    public void retrieveResource() {
        Optional<String> resourcePath = htmlResourceQueue.getElement();
        resourcePath.ifPresent(s -> {
            String result = restTemplateForHtmlResource.getForEntity(s, String.class).getBody();
            HtmlResource htmlResourceToStore = new HtmlResource();
            htmlResourceToStore.setHttpPath(s);
            htmlResourceToStore.setHtmlResource(result);
            htmlResourceRepository.save(htmlResourceToStore);
        });
    }
}
