package com.fraczekkrzysztof.resourceretriever.resource.htmlusecase;

import com.fraczekkrzysztof.resourceretriever.dto.ResourceRequestDto;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.validator.UrlValidationException;
import com.fraczekkrzysztof.resourceretriever.validator.UrlValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("html")
@RequiredArgsConstructor
public class HtmlResourceRestController {

    private final UrlValidator defaultUrlValidator;
    private final ResourceQueue<String> htmlResourceQueue;

    @PostMapping()
    public ResponseEntity<String> postForHtmlResource(@RequestBody @Validated ResourceRequestDto request) throws UrlValidationException {
        String httpPath = request.getUrl();
        defaultUrlValidator.validateUrl(httpPath);
        htmlResourceQueue.addElement(request.getUrl());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
