package com.fraczekkrzysztof.resourceretriever.resource;

public interface ResourceRetriever {

    void retrieveResource();
}
