package com.fraczekkrzysztof.resourceretriever.resource;

import java.util.Optional;
import java.util.Queue;

public abstract class AbstractResourceQueue<T> implements ResourceQueue<T> {

    private final Queue<T> resourceQueue;

    protected AbstractResourceQueue(Queue<T> queue) {
        resourceQueue = queue;
    }

    @Override
    public void addElement(T element) {
        resourceQueue.add(element);
    }

    @Override
    public Optional<T> getElement() {
        return Optional.ofNullable(resourceQueue.poll());
    }
}
