package com.fraczekkrzysztof.resourceretriever.resource;

import java.util.Optional;

public interface ResourceQueue<T> {

    void addElement(T element);

    Optional<T> getElement();
}
