package com.fraczekkrzysztof.resourceretriever.resource.fileusecase;

import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceRetriever;
import com.fraczekkrzysztof.resourceretriever.resource.fileusecase.model.FileResource;
import com.fraczekkrzysztof.resourceretriever.resource.fileusecase.repository.FileResourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FileResourceRetriever implements ResourceRetriever {

    private final ResourceQueue<String> fileResourceQueue;
    private final FileResourceRepository fileResourceRepository;
    private final RestTemplate restTemplateForFileResource;

    @Override
    @Scheduled(fixedRate = 1000)
    public void retrieveResource() {
        Optional<String> resourcePath = fileResourceQueue.getElement();
        resourcePath.ifPresent(path -> {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(List.of(MediaType.ALL));
            byte[] contentOfResource = restTemplateForFileResource.exchange(path, HttpMethod.GET, new HttpEntity<>(headers), byte[].class).getBody();
            FileResource fileResourceToStore = new FileResource();
            fileResourceToStore.setHttpPath(path);
            fileResourceToStore.setFileResource(contentOfResource);
            fileResourceRepository.save(fileResourceToStore);
        });
    }
}
