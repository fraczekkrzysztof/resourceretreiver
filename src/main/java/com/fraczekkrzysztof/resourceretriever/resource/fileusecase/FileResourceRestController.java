package com.fraczekkrzysztof.resourceretriever.resource.fileusecase;

import com.fraczekkrzysztof.resourceretriever.dto.ResourceRequestDto;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import com.fraczekkrzysztof.resourceretriever.validator.UrlValidationException;
import com.fraczekkrzysztof.resourceretriever.validator.UrlValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController()
@RequestMapping("file")
@RequiredArgsConstructor
public class FileResourceRestController {

    private final UrlValidator defaultUrlValidator;
    private final ResourceQueue<String> fileResourceQueue;

    @PostMapping()
    public ResponseEntity<String> postForFileResource(@RequestBody @Valid ResourceRequestDto request) throws UrlValidationException {
        String httpPath = request.getUrl();
        defaultUrlValidator.validateUrl(httpPath);
        fileResourceQueue.addElement(httpPath);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
