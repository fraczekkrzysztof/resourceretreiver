package com.fraczekkrzysztof.resourceretriever.resource.fileusecase.configuration;

import com.fraczekkrzysztof.resourceretriever.resource.AbstractResourceQueue;
import com.fraczekkrzysztof.resourceretriever.resource.ResourceQueue;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ConcurrentLinkedQueue;

@Configuration
public class FileUseCaseConfiguration {

    @Bean
    public ResourceQueue<String> fileResourceQueue() {
        return new AbstractResourceQueue<>(new ConcurrentLinkedQueue<>()) {
        };
    }

    @Bean
    public RestTemplate restTemplateForFileResource() {
        return new RestTemplateBuilder()
                .additionalMessageConverters(new ByteArrayHttpMessageConverter())
                .build();
    }
}
