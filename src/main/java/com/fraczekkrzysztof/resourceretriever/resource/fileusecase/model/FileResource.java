package com.fraczekkrzysztof.resourceretriever.resource.fileusecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class FileResource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "httpPath", updatable = false)
    private String httpPath;

    @Lob()
    @Column(name = "fileResource", updatable = false)
    private byte[] fileResource;

    @CreationTimestamp
    @Column(name = "createdAt", updatable = false)
    private LocalDateTime createdAt;
}
