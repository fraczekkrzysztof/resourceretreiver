package com.fraczekkrzysztof.resourceretriever.resource.fileusecase.repository;

import com.fraczekkrzysztof.resourceretriever.resource.fileusecase.model.FileResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileResourceRepository extends JpaRepository<FileResource, Integer> {
}
