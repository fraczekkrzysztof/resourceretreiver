package com.fraczekkrzysztof.resourceretriever.validator;

public interface UrlValidator {

    void validateUrl(String url) throws UrlValidationException;
}
