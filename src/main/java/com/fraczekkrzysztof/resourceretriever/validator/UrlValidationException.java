package com.fraczekkrzysztof.resourceretriever.validator;

public class UrlValidationException extends Exception {

    public UrlValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
