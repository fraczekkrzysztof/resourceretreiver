package com.fraczekkrzysztof.resourceretriever.validator;

import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

@Component
public class DefaultUrlValidator implements UrlValidator {
    @Override
    public void validateUrl(String url) throws UrlValidationException {
        try {
            new URL(url).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new UrlValidationException(String.format("Provided url \"%s\" is nor valid", url), e);
        }
    }
}
