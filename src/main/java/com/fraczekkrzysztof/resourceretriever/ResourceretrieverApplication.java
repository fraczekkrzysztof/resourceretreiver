package com.fraczekkrzysztof.resourceretriever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ResourceretrieverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResourceretrieverApplication.class, args);
    }

}
