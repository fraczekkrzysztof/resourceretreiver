package com.fraczekkrzysztof.resourceretriever.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ResourceRequestDto {

    @NotBlank(message = "Please provide a valid URL")
    private String url;
}
