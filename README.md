### Resource Retreiver

Aplikacja służy do pobrania zasobów z wskazanego zasobu http. Aplikcja udostępnia REST API

Celem pobrania zasobu HTML należy wykonać POST localhost:8080/resource/html z body
{
    "url" : "https://my-url.com"
}.

Celem pobrania pliku należy wykonać POST localhost:8080/resource/file z body
{
    "url" : "https://my-url.com"
}.


Dev notes:
Nie chcąc komplikować celowo użyłem pamięciowej bazy h2 - jestem świadomy, iż parametry tam podane nie mają prawa znaleźć się w produkcyjnej aplikacji (chociażby ddl-auto=update)
